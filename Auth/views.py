from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required


def index(request):
    return render(request, 'halo.html')


@login_required
def home(request):
    return render(request, 'nama.html')


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()

            return redirect('Auth:nama')
    else:
        form = UserCreationForm()

    return render(request, 'signup.html', {'form': form})
