from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('query/', views.query_buku, name='query'),
    # path('test/', views.test, name='test'),
    # path('demo/', views.demo, name='demo')
]
