from django.shortcuts import render
from django.http import JsonResponse
from requests import get
import json


def home(request):
    return render(request, 'main/home.html')

def query_buku(request):
    keyword = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + keyword
    data = get(url).content
    data = json.loads(data)
    return JsonResponse(data, safe=False)

# def test(request):
#     return render(request,'main/tes.html')
#
# def demo(request):
#     return render(request, 'main/demo.html')
